/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.robotscan;

import java.util.Scanner;

/**
 *
 * @author patthamawan
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in); 
        TableMap map  = new TableMap(10,10);
        robot robot = new robot(2,2,'x',map);
        Bomb bomb = new Bomb(5,5);
        map.setRobot(robot);
        map.setBomb(bomb);
        map.Showmap();
        while(true){
            map.Showmap();
            char direction = inputDirection(kb);
            if(direction=='q'){
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        char direction = str.charAt(0);
        return direction;
    }
}
