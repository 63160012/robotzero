/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.robotscan;

/**
 *
 * @author patthamawan
 */
public class TableMap {
    private int width;
    private int height;
    private robot robot;
    private Bomb bomb;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setRobot(robot Robot) {
        this.robot = Robot;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }
    
    public void Showmap(){
        showTitle();
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++ ){
                if(robot.isOn(x, y)){
                  showrobot();
                }else if(bomb.isOn(x, y)){
                  showBomb();
                }else{
                  showCell();
                }
               
        }
            showNewline();
      }
        
    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void showNewline() {
        System.out.println("");
    }

    private void showCell() {
        System.out.print("-");
    }

    private void showBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void showrobot() {
        System.out.print(robot.getSymbol());
    }
    
    public boolean inMap(int x,int y){
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }
    public boolean isBomb(int x,int y){
        return bomb.isOn(x,y);
    }
}
